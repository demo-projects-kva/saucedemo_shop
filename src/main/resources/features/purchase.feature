Feature: Login with the correct credentials and buy a product

  Scenario: Login with the correct credentials
    Given the online shop "https://www.saucedemo.com/" url is open
    When the user enters credentials "standard_user" "secret_sauce"
    And the user clicks submit button
    Then the "PRODUCTS" page is shown

  Scenario: Buy a product
    Given the product assortment is shown on the page
    When the user clicks on the random product name link
    And the user adds product to the shopping cart
    And the user opens the shopping cart
    And the user proceeds to the checkout
    And the user enters shipping info: "FirstName", "LastName", "ZipCode"
    Then confirmation message is shown
    And the user logs out
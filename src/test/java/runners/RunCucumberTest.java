package runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import static drivers.DriverFactory.getChromeDriver;

@CucumberOptions(features = "src/main/resources/features", glue="steps", publish = true )

public class RunCucumberTest extends AbstractTestNGCucumberTests {

     WebDriver driver = getChromeDriver();

    @BeforeSuite
    public void startUpBrowser(){
        driver = getChromeDriver();
    }

    @AfterSuite
    public void closeBrowser(){
        driver.quit();
    }

}

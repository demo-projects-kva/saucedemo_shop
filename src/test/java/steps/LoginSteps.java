package steps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static drivers.DriverFactory.getChromeDriver;



public class LoginSteps {

    WebDriver driver = getChromeDriver();

    @When("the user enters credentials {string} {string}")
    public void the_user_enters_credentials(String username, String password) {
        driver.findElement(By.id("user-name")).sendKeys(username);
        driver.findElement(By.id("password")).sendKeys(password);
    }

    @When("the user clicks submit button")
    public void the_user_clicks_submit_button() {
        driver.findElement(By.id("login-button")).click();
    }

    @Then("the user logs out")
    public void the_user_loggs_out() {
        driver.findElement(By.id("react-burger-menu-btn")).click();
        driver.findElement(By.id("logout_sidebar_link")).click();
        System.out.println("Logging out");
    }
}

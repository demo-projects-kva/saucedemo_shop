package steps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import static drivers.DriverFactory.getChromeDriver;


public class CheckoutSteps  {

    WebDriver driver = getChromeDriver();

    @When("the user opens the shopping cart")
    public void the_user_opens_the_shopping_cart() {
        driver.findElement(By.id("shopping_cart_container")).click();
        Assert.assertEquals("YOUR CART" , driver.findElement(By.className("title")).getText());
    }

    @When("the user proceeds to the checkout")
    public void the_user_proceeds_to_the_checkout() {
        driver.findElement(By.id("checkout")).click();
        Assert.assertEquals("CHECKOUT: YOUR INFORMATION" , driver.findElement(By.className("title")).getText());
    }

    @When("the user enters shipping info: {string}, {string}, {string}")
    public void the_user_enters(String firstname, String lastname, String zipcode) {
        driver.findElement(By.id("first-name")).sendKeys(firstname);
        driver.findElement(By.id("last-name")).sendKeys(lastname);
        driver.findElement(By.id("postal-code")).sendKeys(zipcode);
        driver.findElement(By.name("continue")).click();
        Assert.assertEquals("CHECKOUT: OVERVIEW" , driver.findElement(By.className("title")).getText());
    }

    @Then("confirmation message is shown")
    public void confirmation_message_is_shown() {
        driver.findElement(By.name("finish")).click();
        Assert.assertEquals("THANK YOU FOR YOUR ORDER" , driver.findElement(By.className("complete-header")).getText());
        Assert.assertEquals("Your order has been dispatched, and will arrive just as fast as the pony can get there!" , driver.findElement(By.className("complete-text")).getText());
    }
}

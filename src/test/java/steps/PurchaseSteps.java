package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static drivers.DriverFactory.getChromeDriver;


public class PurchaseSteps {
    WebDriver driver = getChromeDriver();

    @Given("the product assortment is shown on the page")
     public static ArrayList<String> getProductList() {
        WebDriver driver = getChromeDriver();
        List<WebElement> elements = driver.findElements(By.className("inventory_item"));

        ArrayList<String> products = new ArrayList<>();
        for (WebElement i : elements) {
            String name = i.findElement(By.xpath("./child::div[2]//div[contains(@class,\"name\")]")).getText();
            String price = i.findElement(By.xpath("./child::div[2]/div[2]/div[@class=\"inventory_item_price\"]")).getText();
            String description = i.findElement(By.xpath("./child::div[2]//div[@class=\"inventory_item_desc\"]")).getText();
            products.add(name + ":" + price + ":" + description);
        }
        return products;
    }

    @When("the user clicks on the random product name link")
    public void the_user_clicks_on_the_link() {
        ArrayList<String> availableProducts = getProductList();
        int size = availableProducts.size();
        Random r=new Random();
        int randomNumber = r.nextInt(size);
        String randomChoice = availableProducts.get(randomNumber);
        String[] parts = randomChoice.split(":");
        String name = parts[0];
        String price = parts[1];
        System.out.println("Selected item will be nr: " + randomNumber + " - " + name + " , which costs: " + price);
        driver.findElement(By.linkText(name)).click();
        Assert.assertEquals(name , driver.findElement(By.className("inventory_details_name")).getText());
    }

    @When("the user adds product to the shopping cart")
    public void the_user_adds_to_the_shopping_cart() {
        driver.findElement(By.xpath("//button[text()=\"Add to cart\"]")).click();
        driver.findElement(By.id("back-to-products")).click();
    }

}

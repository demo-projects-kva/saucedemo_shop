package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;


import static drivers.DriverFactory.getChromeDriver;


public class Base {
    WebDriver driver = getChromeDriver();

    @Given("the online shop {string} url is open")
    public void the_online_shop_homepage_is_open(String url) {
        driver.get(url);
  
    }

    @Then("the {string} page is shown")
    public void the_product_page_is_shown(String PageName) {
        Assert.assertEquals(PageName , driver.findElement(By.className("title")).getText());
    }
}

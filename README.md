# ONLINE SHOP DEMO
This is a Test Automation demo for the online shop presentation for Selenium tests with Cucumber under maven. 

Use maven and JDK 1.8 to run the tests<br>
Default browser is "chrome";<br>
Tests are running with TestNG; <br>
I am using IntelliJ IDEA as my IDE, so steps will be described now to be able to run it there.<br>

## Getting started
For running tests on your machine, you need to open your IntelliJ IDEA - you can download it here: [intellijIDEA](https://www.jetbrains.com/idea/download/#section=windows) , if you dont have it already. You can select the Community Edition, which is free for use, if you dont have the licence for the full version.  

After you open the IntelliJ IDEA for the first time, you have few options how to create a project - this time we need to click on `Get from Version Control`.  <br>

![image.png](./image.png)

After that you need to paste copied [url](https://gitlab.com/demo-projects-kva/saucedemo_shop.git) of the project and click `Clone`.
Then you need to enter your credentials, if they are not saved automatically. 
Cloned project already contains dependencies, which are required for the current test run, so you only need to make sure that you have installed the chromedriver.exe

### Testing with Chrome

1. Download and install [chromedriver](https://sites.google.com/chromium.org/driver/downloads?authuser=0) to directory `${HOME}/bin`
2. Expand folder `/src/test/java/runners/` and right click on `Run "RunCucumberTest"` class.


## More about the current tests and architecture
This project contains tests for [saucedemo.com](https://www.saucedemo.com/) online shop.
Tests are written in Gherkin syntax: <br>
First of all, we have the 'Features', which are separated into 'Scenarios', to give the understanding of what kind of steps we need to do as a user, and what we expect to get in the result.<br>

![image-3.png](./image-3.png) <br>
Feature files are located in `/src/main/java/resources/features` folder. At the moment there is only 1 feature, which covers 2 scenarios - succesfull login and buying a product. 
Because this project is very small, it didn't make sense to separate those, but when the project becomes more complicated and contains many features, it is better to filter all features according to some specific logic, for example, we would have a separate feature file for login (where we could put different scenarios, like - 1. succesful login, 2. login with wrong credentials, etc), and separate feature file for buying a product (which also can be divided into smaller parts).

After we have written the file.feature, we need to define the steps -> those are describer in `/src/test/java/steps/` folder. Steps can also be splitted by common logic, to not make the code too long and hard to use. 

And finally, those are comming together to run the tests in the `/src/test/java/runners/` folder, with a `Run "RunCucumberTest"` class.
`@CucumberOptions(features = "src/main/resources/features", glue="steps", publish = true )`


## Description of task
This project should cover the following task: <br>
Provide a runnable automation testsuite for the following user flow:<br>
● Login into the website<br>
● Select a product<br>
● Add to cart<br>
● Proceed to checkout<br>
● Confirm checkout<br>
● Logout (left side menu)<br>
The scenarios should be designed with expected results, following the Gherkin syntax: Given-When-Then

## Visuals
Video of running test you can check on google drive: [googleDrive_video_url](https://drive.google.com/drive/folders/1id1aCWsUIoj6fQStC5iPArvEybq6eQSw?usp=sharing)


## Roadmap
It is missing now to use Selenium Grid and other browsers for test execution.<br>
For the current task, only 1 product is purchased in the scenario, which is randomly selected from the all available products. <br>
Checkout page is not checked for the final price and description, only for the product name. -> can be improved to the full product information check.


## Author
Task is done by Karina Valtere


## Project status

Can be ugraded later if it is neccessary to show the possible imrpovements.<br>
Currently not started the next step of development - Selenium Grid. 
